#/usr/bin/env python

import sys
import praw

def usage():
	print ("usage\n %s <username>"%sys.argv[0])


user_agent = "Atilla's comment collector"
#Reddit API allows maximum 1000 items of any list
MAX_LIST = 1000


def main():
	if len(sys.argv) < 2:
		usage()
		sys.exit(1)
	
	r = praw.Reddit(user_agent=user_agent)
	u = r.get_redditor(sys.argv[1])
	comgen = u.get_comments(limit=MAX_LIST)
	for comment in comgen:
		print (comment.body.encode("UTF-8"))

	
if __name__ == "__main__":
	main()

